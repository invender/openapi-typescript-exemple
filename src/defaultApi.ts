import { DefaultApi, Configuration } from "@openapi-test/ts-lib/src"

import { useState } from "react";

const configuration = new Configuration({
    basePath: "http://localhost:8080",
});

export function useAPIMethod<Params extends unknown[], Response = void>(
    APIMethod: (...params: Params) => Promise<Response>
): [response: Response | null, error: unknown, isLoading: boolean, doRequest: (...params: Params) => void] {
    const [response, setResponse] = useState<Response | null>(null);
    const [error, setError] = useState<unknown>();
    const [loading, setLoading] = useState(false);
    const doRequest = (...params: Params) => {
        setResponse(null);
        setError(undefined);
        setLoading(true);
        APIMethod.apply(defaultApi, params)
            .then((resp) => setResponse(resp))
            .catch((err) => {
                console.error(err);
                setError(err);
            })
            .finally(() => setLoading(false));
    };
    return [response, error, loading, doRequest];
}

export const defaultApi = new DefaultApi(configuration);
