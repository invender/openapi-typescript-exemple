import { useEffect } from "react";
import { defaultApi, useAPIMethod } from "./defaultApi";

function App() {
  const [versionResponce, , , doVersionRequest] = useAPIMethod(
    defaultApi.versionGet
  );
  useEffect(() => {
    doVersionRequest();
  }, []);

  return (
    <div>
      {versionResponce}
    </div>
  );
}

export default App;
